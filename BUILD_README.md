Prerequisites:

   1. Host machine with relevant OS to install cross compiler and packages (Preferrably Ubuntu/Debian)
   2. Rock960 Model AB

Install packages:

 Install the required packages on host by following the instructions below:

         $ sudo apt update
         $ sudo apt install build-essential libncurses5-dev git make

Download GNU cross-toolchain binaries:

 The cross compiler toolchain for the Rock960 board can be downloded using the following steps,

        $ mkdir toolchain
        $ wget releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu/gcc-*-x86_64_aarch64-linux-gnu.tar.xz
        $ tar -xf gcc-*-x86_64_aarch64-linux-gnu.tar.xz -C ./toolchain --strip-components=1

#Linux kernel source

There are two ways to get binaries for Rockchip,

  1. prebuild econ binaries which contain total system.img of Debian with inbuild e-CAM55_CUMI0521_MOD device tree,kernel image and modules
  2. Build from the source files. [kernel ,device tree and modules]  
 

#Deploying the SDCard with binaries

  1. The prebuild source which was given contains all the files (kernel,device tree, modules) for e-CAM50 as inbuild.
     with this binary user can load and stream camera module.Unzip the prebuild binary system.img.zip before flashing it in SD card.

  2. In Rockchip Base binary user have to buil,All-in-one GPT image with bootloader/kernel/rootfs from the following link,
      (https://www.96boards.org/documentation/consumer/rock/downloads/)

  User can deploy SD card with prebuilt binary or Rockchip base binary 

  To know how to flash SD card with binaries, use the following link
  (https://www.96boards.org/documentation/consumer/rock/installation/linux-sd.md.html)

#Build & Update Linux kernel on Rock960 from source

This section provides the instructions for building and deploying images for Rock960 from x86 host machine.

   1. Get the kernel source code from the git repository

         $ git clone https://github.com/96rocks/kernel.git

   2. Move to "96rocks_kernel" directory and checkout to "rock960-4.4-dev" branch as follows,
  
         $ cd 96rocks_kernel
         $ git checkout release-4.4-rock960

   3. Apply ecam50_cu96_kernel.patch to the 96rocks_kernel as follows,

        $ git apply <PATH_TO_KERNEL_PATCH>/ecam50_cu96_kernel_rk_v1_0.patch

       Note: ecam50_cu96_kernel.patch file present inside the "<PATH_TO_ECAM50_CU96_GIT_PROEJCT>/kernel_patch/"

   4. Copy all the driver files from the directory camera_driver/ to the directory 96rocks_kernel/drivers/media/i2c/ 

        $ cp <PATH_TO_ECAM50_CU96_GIT_PROEJCT>/camera_driver/* drivers/media/i2c/

   5. Export the required build variables and cross compiler path as follows,

         $ export ARCH=arm64
         $ export CROSS_COMPILE=<path-to-cross-compiler>/bin/aarch64-linux-gnu-

        [Note: Replace **path-to-cross-compiler** in the above command with the location where you have extracted the toolchain]

   6. Configure the kernel with default configuration file "rockchip_linux_defconfig"

         $ make rockchip_linux_defconfig

   7. Build the kernel and device tree using the following command,

         $ make Image dtbs -j$(nproc)

   8. After compilation, The Image and device tree binary are available in the following directories:

         For Rock960 Model AB:
             <build_Image_directory> - arch/arm64/boot/Image
             <build_device_tree_directory> - arch/arm64/boot/dts/rockchip/rock960-model-ab-linux.dtb

   9. Build Modules using the following commands,
    
        $ make modules -j$(nproc)

   10. create separate directory to install modules using

        $ mkdir ../modules_fir

   11. Install the modules using the following command.

        $ make modules_install INSTALL_MOD_PATH=<path-to-"modules_fir"-directory>

       In "modules_fir/lib/"directory , modules and firmware are available.


##Updating kernel,device tree and modules in the SDCard

The SD card which is deployed with Rockchip base binary or prebuilt binary has 5 patitions. In that the fourth partition contains kernel image ,device tree and extlinux.conf files. The built binary files can be updated using the following steps,

  1. Insert the SD card with binary using card reader, in the host computer and give following command,
 
        $ lsblk

  2. Note device name (/dev/sdX) and mount the fourth partition

    for example:
        $ sudo mount /dev/sdb4 /mnt/
        $ ls /mnt/

    User can see the kernel Image, device tree binary and conf file

  3. Replace the SD card kernel image and device tree with our newly build kernel image and device tree.

        $ sudo cp /<build_image_directory>/Image /mnt/
        $ sudo cp /<build_device_tree_directory>/rock960-model-ab-linux.dtb /mnt/
 
  4. Change the device tree name in extlinux.conf file as rock960-model-ab-linux.dtb     
     After changing the device tree name unmount the partition

        $ sudo umount /mnt

  5.  The /lib directory in the rootfs of (ubuntu or debian)SD card contains a modules and firmware directories
      Replace those directories with the modules and firmware in the lib folder of modules_fir directory obtained during build process

        $ sudo cp <PATH_TO_MODULES_FIR_DIRECTORY>/lib/modules /media/<SD_CARD_NAME>/lib/
        $ sudo cp <PATH_TO_MODULES_FIR_DIRECTORY>/lib/firmware /media/<SD_CARD_NAME>/lib/

  6. copy the initialize.sh file which was given with source to home directory in the rootfs of SD card
      
        $ cp <PATH_TO_PREBUILT_BINARIES_SCRIPTS>/initialize.sh /media/<SD_CARD_NAME>/home/<linaro or rock>/

#Expanding root file system size

  To expand root file system size use the following link
  (https://www.96boards.org/documentation/consumer/rock/guides/expand-rootfs.md.html)

#Loading modules and streaming
 Insert the SD card into Rock960
 As e-cam50 is given as module,it should be loaded on kernel
 
  1. To check loaded modules on kernel

        $ lsmod

  2. To load e-cam50 module on kernel

        $ sudo modprobe ar0521_mcu

  3. To Install media-ctl and v4l2, if it is not available,

        $ sudo apt-get update
        $ sudo apt-get install v4l-utils

  4. To check Media control entities

        $ media-ctl -d /dev/media0 -p

  5. Camera configuration commands and camera to ISP link commands are given in "initialize.sh" shell file. 
     Use following commands to configure camera

        $ chmod +x initialize.sh
        $ ./initialize.sh
 
#Streaming command
 $ gst-launch-1.0 rkcamsrc device=/dev/video0 io-mode=dmabuf ! video/x-raw, width=1920, height=1080, framerate=60/1 ! fpsdisplaysink video-sink=rkximagesink text-overlay=false -v
