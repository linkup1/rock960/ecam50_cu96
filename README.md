##e-CAM50 camera driver for Rock960

Copyright © 2020 e-con Systems India Pvt. Limited
All rights reserved.

####VERSION

  LinkUP_rk_v1.0

####FEATURES:  
 e-con systems now provide camera modules for Rock960 boards.  

 Rock960 is a community board based on SoC RK3399. It is the first 96 board with rockchip processor.This boards are used in industrial and development purposes.  

 e-CAM55_CUMI0521_MOD camera module is a 5MP camera with MIPI connector designed and developed by e-con systems. The driver for this camera module supports all boards with SoC RK3399.  
 
 Camera module(e-CAM55_CUMI0521_MOD), camera adapter board (e-CAM130_TRICUTX2_ADAPTOR) and camera base board (ACC_96DEV_MIPICAMERA_ADP) are collectively called as e-CAM50_CU96.  
 
 1.e-CAM55_CUMI0521_MOD camera driver supports following resolutions,  
     1280x720 @70fps  
     1920x1080 @60fps  
     2560x1440 @35fps  
     2592x1944 @25fps  

 2.Image quality controls to enable adjustments  
     Brightness  
     Contrast  
     Saturation  
     Gamma  
     Gain  
     Sharpness  
     White_balance controls  
     Flip  
     Tilt and Pan  
     Zoom  
     Exposure - Auto,Manual,Roi based  
     Exposure compensation  
     Denoise  


####PREREQUISITE  
 1.ROCK960 board  
 2.e-CAM55_CUMI0521_MOD (REV X4)  
 3.e-CAM130_TRICUTX2_ADAPTOR (REV B)  
 4.ACC_96DEV_MIPICAMERA_ADP (REV X1) camera adapter developed by e-con systems.  
     

