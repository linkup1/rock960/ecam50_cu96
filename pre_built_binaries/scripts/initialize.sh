#!/bin/bash
#
#camera configuration script

media-ctl -d /dev/media0 --set-v4l2 '"rkisp1-isp-subdev":2[fmt:UYVY2X8/1920x1080 field:none]'

if [ $? != 0 ]; then
  echo Configuration Command 1 Failed
  exit 1
fi

media-ctl -d /dev/media0 --set-v4l2 '"rkisp1-isp-subdev":0[fmt:UYVY2X8/1920x1080 field:none]'

if [ $? != 0 ]; then
  echo Configuration Command 2 Failed
  exit 1
fi

media-ctl -d /dev/media0 --set-v4l2 '"ar0521_mcu 2-0042":0[fmt:UYVY2X8/1920x1080 field:none]'

if [ $? != 0 ]; then
  echo Configuration Command 3 Failed
  exit 1
fi

#Camera and ISP link command

media-ctl -d /dev/media0 -l '"ar0521_mcu 2-0042":0->"rockchip-sy-mipi-dphy":0[1]'

if [ $? != 0 ]; then
  echo Configuring camera and ISP link Failed
  exit 1
fi
