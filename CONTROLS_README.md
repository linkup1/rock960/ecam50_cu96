#Controls

1. To get informations about the camera driver.
  $ v4l2-ctl -d 0 -D -V

2. To list the controls supported by the camera driver.
  $ v4l2-ctl -d 0 --list-ctrls

3. To get the current camera control value.
  Examples:
  $ v4l2-ctl -d 0 --get-ctrl=exposure_auto
  $ v4l2-ctl -d 0 --get-ctrl=zoom_absolute

4. To set the camera control with required value.
  Examples:
  $ v4l2-ctl -d 0 --set-ctrl=zoom_absolute=300
  $ v4l2-ctl -d 0 --set-ctrl=brigntness=5

5. To set camera in manual exposure mode with required exposure value.

  $ v4l2-ctl -d 0 --set-ctrl=exposure_auto=1
  $ v4l2-ctl -d 0 --set-ctrl=exposure_time_absolute=333
